**PHP tiny Timer class** to measure performance of several PHP code items.
This class is a singleton so you can access it from anywhere.

Outputs each step with time it took, total time and a description in the apache error_log().
To output to screen instead, simply change the error_log(...); to print ...;

**Usage example:**

```
#!php

require_once("Timer.class.php");
Timer::start();
// Do something
Timer::step('Done prepping');
// Do something
Timer::step('Done creating PDF document');
```


**Output example:**

Time step: 0.002 - total: 0.002 - task: prep

Time step: 0.003 - total: 0.005 - task: calculations done

Time step: 1.220 - total: 1.225 - task: PDF document created