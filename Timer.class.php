<?php
////////////////////////////////////////////////////////////////////////
//
//   ___     _                  ___       __ _                       
//  | __|  _| |_ _  _ _ _ ___  / __| ___ / _| |___ __ ____ _ _ _ ___ 
//  | _| || |  _| || | '_/ -_) \__ \/ _ \  _|  _\ V  V / _` | '_/ -_)
//  |_| \_,_|\__|\_,_|_| \___| |___/\___/_|  \__|\_/\_/\__,_|_| \___|
//                                                                  
// 
// 2016-07-01 :: futuresoftware.nl
//////////////////////////////////////////////////////////////////////// 

class Timer {
    private static $instance;
    private static $start;
    private static $last;

    public static function start() {
        $c = self::singleton();
        $c->start = $c->get();
        $c->last = $c->get();
    }

    public static function step($task='') {
        $c = self::singleton();
        $fl = $c->last;
        $c->last = self::get();
        $diffL = round(($c->last - $fl),3);
        $diffT = round(($c->last - $c->start),3);
        error_log("Time step: {$diffL} - total: {$diffT} - task: {$task}");
    }

    private static function get() {
        return microtime(true);
    }

    private static function singleton() {
        if (!isset(self::$instance)) {
            self::$instance = new Timer;
        }
        return self::$instance;
    }
}

